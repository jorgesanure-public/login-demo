<?php

namespace App\Controllers;

use App\Services\IAuthenticator;
use App\Views\IView;

class IndexController implements IIndexController
{
    private IAuthenticator $authenticator;
    private IView $view;

    public function __construct(IAuthenticator $authenticator, IView $view)
    {
        $this->authenticator = $authenticator;
        $this->view = $view;
    }

    public function index(): void
    {
        $this->view->build('index/index.php', [
            'authenticator' => $this->authenticator
        ]);
    }
}