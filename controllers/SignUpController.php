<?php

namespace App\Controllers;

use App\Constants\SessionKeys;
use App\Exceptions\UsernameAlreadyExistsException;
use App\Logs\ILogger;
use App\Models\UserModel;
use App\Repositories\IUserRepository;
use App\Services\IAuthenticator;
use App\Services\IRequest;
use App\Services\IResponseHandler;
use App\Services\ISession;
use App\Views\IView;

class SignUpController extends BaseController implements ISignUpController
{
    private IUserRepository $userRepository;
    private IView $view;

    public function __construct(IUserRepository $userRepository, IAuthenticator $authenticator, IRequest $request, IResponseHandler $responseHandler, ISession $session, ILogger $logger, IView $view)
    {
        $this->userRepository = $userRepository;
        $this->view = $view;
        parent::__construct($authenticator, $request, $responseHandler, $logger, $session);
    }

    public function showSignUpForm(): void
    {
        $userErrorMsg = $this->session->getOnce(SessionKeys::USER_ERROR_MESSAGE);
        $username = $this->session->getOnce('signUpUsername');

        $this->view->build('signup/signup_form.php', [
            'userErrorMsg' => $userErrorMsg,
            'signUpUsername' => $username
        ]);
    }

    public function signup(): void
    {
        $givenUsername = $this->request->getFromPostParams('username');
        $givenPassword = $this->request->getFromPostParams('password');

        if (is_string($givenUsername) === false) {
            $givenUsername = '';
        }
        if (is_string($givenPassword) === false) {
            $givenPassword = '';
        }

        $hashedGivenPassword = password_hash($givenPassword, PASSWORD_BCRYPT);

        $userModel = new UserModel($givenUsername, $hashedGivenPassword, 'account');

        try {
            $this->userRepository->signUp($userModel);
            $this->authenticator->authenticate($givenUsername);
        } catch (UsernameAlreadyExistsException $th) {
            $this->session->set(SessionKeys::USER_ERROR_MESSAGE, 'Username already exists');
            $this->session->set('signUpUsername', $givenUsername);
        } catch (\Throwable $th) {
            $this->logger->logError($th->getMessage(), $th->getFile(), $th->getLine());
            $this->session->set(SessionKeys::USER_ERROR_MESSAGE, 'Something went wrong, try again later.');
        }

        if ($this->authenticator->isAuthenticated()) {
            $this->responseHandler->resolve(function() {
                $this->request->redirectTo('/dashboard');
            });
        } else {
            $this->responseHandler->reject(function() {
                $this->request->redirectTo('/signup');
            });
        }
    }
}