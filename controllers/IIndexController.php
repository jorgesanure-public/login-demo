<?php

namespace App\Controllers;

interface IIndexController
{
    public function index(): void;
}