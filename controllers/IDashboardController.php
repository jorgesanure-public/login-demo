<?php

namespace App\Controllers;

interface IDashboardController
{
    public function index(): void;
}