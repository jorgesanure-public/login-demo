<?php

namespace App\Controllers;

use App\Logs\IDeployLogger;
use App\Services\IEnv;
use App\Services\IRequest;
use App\Services\IResponse;
use App\Services\IRobots;
use App\Services\ISitemap;

class DeployController implements IDeployController
{
    private IDeployLogger $deployLogger;
    private IRequest $request;
    private IResponse $response;
    private ISitemap $sitemap;
    private IRobots $robots;
    private IEnv $env;

    public function __construct(IDeployLogger $deployLogger, IRequest $request, IResponse $response, ISitemap $sitemap, IRobots $robots, IEnv $env)
    {
        $this->deployLogger = $deployLogger;
        $this->request = $request;
        $this->response = $response;
        $this->sitemap = $sitemap;
        $this->robots = $robots;
        $this->env = $env;
    }

    public function deploy(): void
    {
        if (
            $this->request->getUriWithoutParams() === '/deploy'
            && $this->request->isPost()
            && $this->request->getFromServer('HTTP_X_GITLAB_INSTANCE') === $this->env->get('X_GITLAB_INSTANCE')
            && $this->request->getFromServer('HTTP_X_GITLAB_TOKEN') === $this->env->get('X_GITLAB_TOKEN')
	    ) {
            $this->pullChangesFromGit();
            $this->applyMigrations();
            $this->runSeeder();
            $this->robots->createRobotsFile();
            $this->sitemap->createSitemapFile();
            $this->response->responseOK();
        } else {
            $this->response->responseNotFound();
        }
    }

    private function pullChangesFromGit(): void
    {
        $cmd = 'cd ' . __DIR__ . '/../ && git pull origin production && cd controllers';
        $result = shell_exec($cmd);
        if ($result === false || $result === null) {
            $result = '';
        }
        $this->deployLogger->log('[DEPLOY] Pulling changes from git');
        $this->deployLogger->log($cmd);
        $this->deployLogger->log($result);
        $this->deployLogger->log('[DEPLOY] Changes have been applied.');
    }

    private function applyMigrations(): void
    {
        $cmd = 'source ' . __DIR__ . '/../../../.bashrc && php ' . __DIR__ . '/../vendor/bin/phinx migrate -e production';
        $result = shell_exec($cmd);
        if ($result === false || $result === null) {
            $result = '';
        }
        $this->deployLogger->log('[MIGRATION] Applying migrations...');
        $this->deployLogger->log($cmd);
        $this->deployLogger->log($result);
        $this->deployLogger->log('[MIGRATION] Migration have been applied.');
    }

    private function runSeeder(): void
    {
        $cmd = 'source ' . __DIR__ . '/../../../.bashrc && php ' . __DIR__ . '/../vendor/bin/phinx seed:run -e production';
        $result = shell_exec($cmd);
        if ($result === false || $result === null) {
            $result = '';
        }
        $this->deployLogger->log('[SEEDER] Applying Seeders...');
        $this->deployLogger->log($cmd);
        $this->deployLogger->log($result);
        $this->deployLogger->log('[SEEDER] Seeders have been applied.');
    }
}
