<?php

namespace App\Controllers;

use App\Logs\ILogger;
use App\Services\IAuthenticator;
use App\Services\IRequest;
use App\Services\IResponseHandler;
use App\Services\ISession;

abstract class BaseController
{
    protected IAuthenticator $authenticator;
    protected IRequest $request;
    protected IResponseHandler $responseHandler;
    protected ILogger $logger;
    protected ISession $session;

    public function __construct(IAuthenticator $authenticator, IRequest $request, IResponseHandler $responseHandler, ILogger $logger, ISession $session)
    {
        $this->authenticator = $authenticator;
        $this->request = $request;
        $this->responseHandler = $responseHandler;
        $this->logger = $logger;
        $this->session = $session;
    }
}