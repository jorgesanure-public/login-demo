<?php

namespace App\Controllers;

interface IDeployController
{
    public function deploy(): void;
}