<?php

namespace App\Controllers;

interface ISignUpController
{
    public function showSignUpForm(): void;
    public function signUp(): void;
}