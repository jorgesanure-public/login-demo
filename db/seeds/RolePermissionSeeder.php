<?php


use Phinx\Seed\AbstractSeed;

class RolePermissionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('role_permissions');

        $table->truncate();

        $permissionCodeList = ['SOUD', 'DOU', 'EOU', 'LU'];
        $roleCodeList = ['SA', 'A'];

        $data = [];
        foreach ($roleCodeList as $roleCode) {
            foreach ($permissionCodeList as $permissionCode) {
                $data[] = [
                    'role_code' => $roleCode,
                    'permission_code' => $permissionCode
                ];
            }
        }

        $table->insert($data)
            ->saveData();
    }
}
