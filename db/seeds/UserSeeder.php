<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $table = $this->table('users');

        $table->truncate();
        
        $data = [
            [
                'username'    => 'jorge',
                'password' => 'password',
                'account_code' => 'JSU',
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        $table->insert($data)
              ->saveData();
    }
}
