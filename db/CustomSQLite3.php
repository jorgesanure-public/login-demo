<?php

namespace App\Db;

use App\Logs\IDataSourceLogger;
use App\Logs\ILogger;
use App\Services\IEnv;
use Exception;
use SQLite3;
use SQLite3Result;
use SQLite3Stmt;

class CustomSQLite3 extends SQLite3
{
    private ILogger $logger;
    private IDataSourceLogger $dataSourceLogger;
    private IEnv $env;

    /**
     * @param string $sqlite3Filename Default: null
     * @param ILogger $logger
     * @param IDataSourceLogger $dataSourceLogger
     */
    public function __construct(string $sqlite3Filename = null, ILogger $logger, IDataSourceLogger $dataSourceLogger, IEnv $env)
    {
        $this->logger = $logger;
        $this->dataSourceLogger = $dataSourceLogger;
        $this->env = $env;
        
        if (empty($sqlite3Filename)) {
            $db = $this->env->get('PHINX_DEFAULT_DATABASE');
            if (empty($db)) {
                throw new Exception("Env 'PHINX_DEFAULT_DATABASE' is empty", 1);
            }
            $sqlite3Filename = __DIR__ . "/$db.sqlite3";
        }

        try {
            parent::__construct($sqlite3Filename);
        } catch (\Throwable $th) {
            $this->logger->logError("[Error] sqlite3Filename: $sqlite3Filename");
            throw $th;
        }
    }

    public function customPrepare(string $query, string $file, int $line): ?CustomSQLite3Stmt
    {
        $customStmt = null;

        $stmt = parent::prepare($query);

        if ($stmt === false) {
            if ($this->lastErrorCode() !== 0) {
                $this->logger->logError($this->lastErrorMsg(), $file, $line);
            }
        } else {
            $customStmt = new CustomSQLite3Stmt($this->logger, $this->dataSourceLogger);
            $customStmt->sqlite3Stmt = $stmt;
            $customStmt->customSqlite3 = $this;
            $customStmt->file = $file;
            $customStmt->line = $line;
        }

        return $customStmt;
    }
}

class CustomSQLite3Stmt
{
    public CustomSQLite3 $customSqlite3;
    public SQLite3Stmt $sqlite3Stmt;
    public string $file;
    public int $line;
    public IDataSourceLogger $dataSourceLogger;
    public ILogger $logger;
    /** @var array<string, mixed> $sqlParams */
    public array $sqlParams;

    public function __construct(ILogger $logger, IDataSourceLogger $dataSourceLogger)
    {
        $this->logger = $logger;
        $this->dataSourceLogger = $dataSourceLogger;
        $this->sqlParams = [];
    }

    public function execute(): SQLite3Result|false
    {
        $fromTime = microtime(true);

        $result = $this->sqlite3Stmt->execute();

        $elapsedTime = number_format(microtime(true) - $fromTime, 5);

        $sqlLogMsg = "[$elapsedTime/s] " . $this->sqlite3Stmt->getSQL();
        $sqlLogParams = '';
        
        $this->dataSourceLogger->log($sqlLogMsg);
        if (! empty($this->sqlParams)) {
            $sqlLogParams = json_encode($this->sqlParams);
            $this->dataSourceLogger->log($sqlLogParams);
        }

        if ($this->customSqlite3->lastErrorCode() !== 0) {
            $this->logger->logError($sqlLogMsg);
            if (! empty($sqlLogParams)) {
                $this->logger->logError($sqlLogParams);
            }
            $this->logger->logError($this->customSqlite3->lastErrorMsg(), $this->file, $this->line);
        }

        return $result;
    }

    public function bindValue(string|int $param, mixed $value, int $type = SQLITE3_TEXT): bool
    {
        $this->sqlParams[$param] = $value;
        return $this->sqlite3Stmt->bindValue($param, $value, $type);
    }

    public function close(): bool
    {
        return $this->sqlite3Stmt->close();
    }
}