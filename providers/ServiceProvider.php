<?php

namespace App\Providers;

use App\Constants\ServiceLifetimeType;
use App\Exceptions\UnknownParamTypeException;
use Exception;
use ReflectionClass;

class Service
{
    public string $lifetimeType;
    public string $class;
    public mixed $concreteClass;

    public function __construct(string $class, mixed $concreteClass, string $lifetimeType = ServiceLifetimeType::SINGLETON)
    {
        $this->class = $class;
        $this->concreteClass = $concreteClass;
        $this->lifetimeType = $lifetimeType;
    }
}

interface IServiceProvider
{
    public function set(string $class, mixed $concreteClass = null, string $lifetimeType = ServiceLifetimeType::TRANSIENT): void;
    public function resolve(string $class): mixed;
}

class ServiceProvider implements IServiceProvider
{
    /** @var array<string, mixed> $serviceList */
    private array $serviceList;
    /** @var array<string, mixed> $singletonServiceList */
    private array $singletonServiceList;

    private const INT = 'int';
    private const FLOAT = 'float';
    private const BOOL = 'bool';
    private const STRING = 'string';

    public function __construct()
    {
        $this->serviceList = [];
        $this->singletonServiceList = [];
    }

    public function set(string $class, mixed $concreteClass = null, string $lifetimeType = ServiceLifetimeType::TRANSIENT, ?bool $allowOverride = true): void
    {
        if (! $allowOverride && array_key_exists($class, $this->serviceList)) {
            throw new Exception("Service '$class' already registered", 1);
        }

        if ($concreteClass === null) {
            $concreteClass = $class;
        }

        $this->serviceList[$class] = new Service($class, $concreteClass, $lifetimeType);
    }

    public function validServiceList(): void
    {
        foreach ($this->serviceList as $service) {
            $concreteClass = $service->concreteClass;

            if (! is_string($concreteClass)) {
                continue;
            }

            require_once (new ReflectionClass($concreteClass))->getFileName();
            /** @phpstan-ignore-next-line */
            $reflectionClass = new ReflectionClass($concreteClass);
            $constructor = $reflectionClass->getConstructor();

            if ($constructor !== null) {
                $params = $constructor->getParameters();
                $docComment = $constructor->getDocComment();
                if ($docComment === false) {
                    $docComment = '';
                }
    
                foreach ($params as $param) {
                    $type = $param->getType();
                    
                    if ($type !== null) {
                        $paramName = $param->getName();
                        /** @phpstan-ignore-next-line */
                        $typeName = $type->getName();

                        $isRegistered = array_key_exists($typeName, $this->serviceList);

                        $hasDefaultValue = 
                            (
                                $this->isScalar($typeName)
                                && $this->hasParamDefaultValueByDoc($docComment, $concreteClass, $paramName, $typeName)
                            )
                            || $param->isDefaultValueAvailable();

                        if (! $hasDefaultValue && ! $isRegistered) {
                            throw new Exception("class '$concreteClass' param '$paramName' type '$typeName' : Registration in service list or default value is required.", 1);
                        }
                    } else {
                        throw new UnknownParamTypeException($concreteClass, $param->getName(), 1);
                    }
                }
            }
        }
    }

    private function hasParamDefaultValueByDoc(string $docComment, string $concreteClass, string $paramName, string $paramType): bool
    {
        $hasDefault = true;

        try {
            $this->getParamDefaultValueByDoc($docComment, $concreteClass, $paramName, $paramType);
        } catch (\Throwable $th) {
            $hasDefault = false;
        }

        return $hasDefault;
    }

    private function getParamDefaultValueByDoc(string $docComment, string $concreteClass, string $paramName, string $paramType): string|null
    {
        $defaultValue = null;

        if ($this->isScalar($paramType)) {
            $matches = [];
            $pattern = '/@param\s+' . $paramType . '\s+\$' . $paramName . '\s+Default:\s+(.+)/';

            if (preg_match($pattern, $docComment, $matches)) {
                $defaultValue = trim($matches[1]);
                if ($defaultValue === 'null') {
                    $defaultValue = null;
                }
            } else {
                throw new Exception("class '$concreteClass' param '$paramName' type '$paramType' has not default value in annotations docblock. Include it using following format to solve it: '@param [int|float|string|bool] \$paramName Default: null'", 1);
            }
        }

        return $defaultValue;
    }

    private function isScalar(string $typeName): bool
    {
        return in_array($typeName, [self::INT, self::BOOL, self::FLOAT, self::STRING]);
    }

    public function resolve(string $class): mixed
    {
        if (! array_key_exists($class, $this->serviceList)) {
            throw new Exception("Service '$class' is not registered", 1);
        }

        /** @var Service $service */
        $service = $this->serviceList[$class];

        if (
            $service->lifetimeType === ServiceLifetimeType::SINGLETON
            && array_key_exists($class, $this->singletonServiceList)
        ) {
            $instance = $this->singletonServiceList[$class];
        } else {
            $concreteClass = $service->concreteClass;
    
            $instance = null;
    
            if (is_callable($concreteClass)) {
                $instance = $concreteClass();
            } else {
                /** @phpstan-ignore-next-line */
                $reflectionClass = new ReflectionClass($concreteClass);
                $constructor = $reflectionClass->getConstructor();
        
                $constructorParams = [];
    
                if ($constructor !== null) {
                    $params = $constructor->getParameters();
                    $docComment = $constructor->getDocComment();
                    if ($docComment === false) {
                        $docComment = '';
                    }
        
                    foreach ($params as $param) {
                        $paramValue = null;
        
                        if ($param->isDefaultValueAvailable()) {
                            $paramValue = $param->getDefaultValue();
                        } else {
                            $type = $param->getType();
                            
                            if ($type !== null) {
                                /** @phpstan-ignore-next-line */
                                $typeName = $type->getName();
                                $paramName = $param->getName();

                                if ($this->isScalar($typeName)) {
                                    $paramValue = $this->getParamDefaultValueByDoc($docComment, $class, $paramName, $typeName);
                                } else {
                                    $paramValue = $this->resolve($typeName);
                                }
                            } else {
                                throw new UnknownParamTypeException($class, $param->getName());
                            }
                        }
        
                        $constructorParams[] = $paramValue;
                    }
                }
        
                $instance = $reflectionClass->newInstanceArgs($constructorParams);
            }
    
            if (empty($instance)) {
                throw new Exception("Couldn't get instance for '$class'", 1);
            } else {
                if (
                    $service->lifetimeType === ServiceLifetimeType::SINGLETON
                    && ! array_key_exists($class, $this->singletonServiceList)
                ) {
                    $this->singletonServiceList[$class] = $instance;
                }
            }
        }

        return $instance;
    }
}