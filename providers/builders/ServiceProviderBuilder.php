<?php

namespace App\Providers\Builders;

use App\Constants\ServiceLifetimeType;
use App\Controllers\DashboardController;
use App\Controllers\DeployController;
use App\Controllers\IDashboardController;
use App\Controllers\IDeployController;
use App\Controllers\IIndexController;
use App\Controllers\ILoginController;
use App\Controllers\IndexController;
use App\Controllers\ISignUpController;
use App\Controllers\LoginController;
use App\Controllers\SignUpController;
use App\Db\CustomSQLite3;
use App\Logs\AccessLogger;
use App\Logs\DataSourceLogger;
use App\Logs\DeployLogger;
use App\Logs\IAccessLogger;
use App\Logs\IDataSourceLogger;
use App\Logs\IDeployLogger;
use App\Logs\ILogger;
use App\Logs\IOldLogFileRemover;
use App\Logs\Logger;
use App\Logs\OldLogFileRemover;
use App\Models\Builders\IUserModelBuilder;
use App\Models\Builders\UserModelBuilder;
use App\Providers\ServiceProvider;
use App\Repositories\IUserRepository;
use App\Repositories\UserRepository;
use App\Routes\IRouter;
use App\Routes\Router;
use App\Services\ArrayUtils;
use App\Services\Authenticator;
use App\Services\CustomEnv;
use App\Services\IArrayUtils;
use App\Services\IAuthenticator;
use App\Services\IEnv;
use App\Services\IRequest;
use App\Services\IResponse;
use App\Services\IResponseHandler;
use App\Services\IRobots;
use App\Services\ISession;
use App\Services\ISitemap;
use App\Services\IStringUtils;
use App\Services\Request;
use App\Services\Response;
use App\Services\ResponseHandler;
use App\Services\Robots;
use App\Services\Session;
use App\Services\Sitemap;
use App\Services\StringUtils;
use App\Views\IView;
use App\Views\View;

class ServiceProviderBuilder
{
    private static ?ServiceProvider $serviceProvider;

    public function __construct()
    {
        self::$serviceProvider = null;
    }

    public function build(): ?ServiceProvider
    {
        $serviceProvider = null;

        if (! empty(self::$serviceProvider)) {
            $serviceProvider = self::$serviceProvider;
        } else {
            $serviceProvider = new ServiceProvider();
            
            /**
             * service register setting
             */
            // Utils
            $serviceProvider->set(IEnv::class, CustomEnv::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->set(IEnv::class, CustomEnv::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->set(IArrayUtils::class, ArrayUtils::class);
            $serviceProvider->set(IStringUtils::class, StringUtils::class);
            $serviceProvider->set(ISession::class, Session::class);
            $serviceProvider->set(IAuthenticator::class, Authenticator::class);
            $serviceProvider->set(IRequest::class, Request::class);
            $serviceProvider->set(IResponse::class, Response::class);
            $serviceProvider->set(IResponseHandler::class, ResponseHandler::class);
            $serviceProvider->set(IRouter::class, Router::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->set(ISitemap::class, Sitemap::class);
            $serviceProvider->set(IRobots::class, Robots::class);
            $serviceProvider->set(IView::class, View::class);
            // Loggers
            $serviceProvider->set(IOldLogFileRemover::class, OldLogFileRemover::class);
            $serviceProvider->set(ILogger::class, Logger::class);
            $serviceProvider->set(IDeployLogger::class, DeployLogger::class);
            $serviceProvider->set(IDataSourceLogger::class, DataSourceLogger::class);
            $serviceProvider->set(IAccessLogger::class, AccessLogger::class);
            // Parsers
            $serviceProvider->set(IUserModelBuilder::class, UserModelBuilder::class);
            // Repositories
            $serviceProvider->set(CustomSQLite3::class, CustomSQLite3::class, ServiceLifetimeType::SINGLETON);
            $serviceProvider->set(IUserRepository::class, UserRepository::class);
            // Controllers
            $serviceProvider->set(ISignUpController::class, SignUpController::class);
            $serviceProvider->set(ILoginController::class, LoginController::class);
            $serviceProvider->set(IIndexController::class, IndexController::class);
            $serviceProvider->set(IDeployController::class, DeployController::class);
            $serviceProvider->set(IDashboardController::class, DashboardController::class);

            $serviceProvider->validServiceList();

            self::$serviceProvider = $serviceProvider;
        }

        return $serviceProvider;
    }
}