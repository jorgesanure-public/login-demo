<?php

namespace App\Services;

interface IResponseHandler
{
    public function resolve(?callable $defaultCallback = null): void;
    public function reject(?callable $defaultCallback = null): void;
    public function setData(mixed $data): IResponseHandler;
    public function getData(): mixed;
    public function getResponse(): IResponse;
}