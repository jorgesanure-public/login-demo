<?php

namespace App\Services;

interface IEnv
{
    public function init(): void;
    public function get(string $key, ?string $default = null): ?string;
    public function set(string $key, ?string $value): void;
}