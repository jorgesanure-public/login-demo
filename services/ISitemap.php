<?php

namespace App\Services;

interface ISitemap
{
    public function createSitemapFile(): void;
}