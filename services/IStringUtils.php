<?php

namespace App\Services;

interface IStringUtils
{
    public function removeBreaklines(?string $str, string $replaceWith = ''): ?string;
    public function removeExtraWhitespace(?string $str): ?string;
    public function buildString(mixed $val): ?string;
}