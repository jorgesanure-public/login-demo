<?php

namespace App\Services;

class Response implements IResponse
{
    public function buildResponseMessage(mixed $message): void
    {
        if(
            ! empty($message)
            && (
                ! is_string($message)
                || ! empty(trim($message))
            )
        ){
            @header('Content-Type: application/json');
            echo json_encode([
                'message' => $message
            ]);
        }else{
            echo '';
        }

        exit;
    }
    
    public function responseBadRequest(mixed $message = 'Bad Request'): void
    {
        http_response_code(400);
        $this->buildResponseMessage($message);
    }
    
    public function responseNotFound(mixed $message = 'Not Found'): void
    {
        http_response_code(404);
    }
    
    public function responseUnauthorized(mixed $message = 'Unauthorized'): void
    {
        http_response_code(401);
        $this->buildResponseMessage($message);
    }
    
    public function responseUnexpectedErrorForUser(): void
    {
        $this->responseUnexpectedError('Something went wrong. Please try again or contact support.');
    }
    
    public function responseUnexpectedError(mixed $message = ''): void
    {
        http_response_code(500);
        if (! empty($message)) {
            $this->buildResponseMessage($message);
        }
        exit;
    }
    
    public function responseOK(mixed $message = ''): void
    {
        http_response_code(200);
        if (! empty($message)) {
            $this->buildResponseMessage($message);
        }
        exit;
    }
}