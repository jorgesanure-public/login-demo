<?php

namespace App\Services;

interface IRobots
{
    public function createRobotsFile(): void;
}