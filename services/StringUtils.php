<?php

namespace App\Services;

class StringUtils implements IStringUtils
{
    public function removeBreaklines(?string $str, string $replaceWith = ''): ?string
    {
        if (! is_null($str)) {
            $str = preg_replace('/[\r\n]+/', $replaceWith, $str);
        }

        return $str;
    }

    public function removeExtraWhitespace(?string $str): ?string
    {
        return empty($str) ? $str : preg_replace('/\s{2,}/', ' ', $str);
    }

    public function buildString(mixed $val): ?string
    {
        if (is_string($val) === false) {
            $val = null;
        }

        return $val;
    }
}