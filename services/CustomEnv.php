<?php

namespace App\Services;

use Exception;

class CustomEnv implements IEnv
{
    /** @var array<string, ?string> $envVars */
    private array $envVars;

    public function __construct()
    {
        $this->envVars = [];
    }

    public function init(): void
    {
        $envFilename = __DIR__ . '/../.env';

        if (file_exists($envFilename)) {
            $fh = fopen($envFilename, 'r');

            if ($fh !== false) {
                while (! feof($fh)) {
                    $line = fgets($fh);
                    
                    if ($line !== false) {
                        $line = trim($line);
        
                        $pattern = '/^([^=]+)=(.+)/';
        
                        if (preg_match($pattern, $line, $matches)) {
                            $this->set($matches[1], $matches[2]);
                        }
                    }
                }
            }
        }
    }

    public function get(string $key, ?string $default = null): ?string
    {
        $envVar = $default;

        if (empty($this->envVars)) {
            $this->init();
        }

        if (array_key_exists($key, $this->envVars)) {
            $envVar = $this->envVars[$key];
        } else {
            throw new Exception("Env '$key' was not found in .env file", 1);
        }

        return $envVar;
    }

    public function set(string $key, ?string $value): void
    {
        $this->envVars[$key] = $value;
    }
}