<?php

namespace App\Services;

class Session implements ISession
{
    private IArrayUtils $arrayUtils;

    public function __construct(IArrayUtils $arrayUtils)
    {
        $this->arrayUtils = $arrayUtils;
    }

    public function get(?string $key): mixed
    {
        return $this->arrayUtils->getValueByKeyFromArray($key, $_SESSION ?? []);
    }

    public function getOnce(?string $key): mixed
    {
        $value = $this->arrayUtils->getValueByKeyFromArray($key, $_SESSION ?? []);

        if (! empty($value)) {
            $this->set($key, null);
        }

        return $value;
    }

    public function set(?string $key, mixed $value): void
    {
        if ($key !== null) {
            $_SESSION[$key] = $value;
        }
    }

    public function clear(): void
    {
        $_SESSION = [];
        session_destroy();
    }
}