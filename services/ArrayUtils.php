<?php

namespace App\Services;

class ArrayUtils implements IArrayUtils
{
    public function getValueByKeyFromArray(?string $key, ?array $array, bool $trimFlag = true): mixed
    {
        return $key !== null && $array !== null && array_key_exists($key, $array) ? ($trimFlag && is_string($array[$key]) ? trim($array[$key]) : $array[$key]) : null;
    }
}