<?php

namespace App\Services;

interface IRequest
{
    public function isPost(): bool;
    public function isGet(): bool;
    public function getMethod(): ?string;
    public function getUri(): ?string;
    public function getRequestSchema(): ?string;
    public function getHttpHost(): ?string;
    public function getHostWithSchema(): string;
    public function getFromServer(?string $key): mixed;
    public function getFromPostParams(?string $key): mixed;
    public function getFromGetParams(?string $key): mixed;
    /** @param array<mixed> $data */
    public function redirectTo(string $routeName, ?array $data = null): void;
    public function isApi(): bool;
    public function getUriWithoutParams(): string;
}