<?php

namespace App\Services;

class Authenticator implements IAuthenticator
{
    private ISession $session;

    public function __construct(ISession $session)
    {
        $this->session = $session;
    }

    public function authenticate(?string $username): void
    {
        $this->session->set('username', $username);
        $this->session->set('isAuthenticated', true);
    }

    public function isAuthenticated(): bool
    {
        return
            (bool) $this->session->get('username')
            && (bool) $this->session->get('isAuthenticated');
    }

    public function getUsername(): string
    {
        $username = $this->session->get('username') ?? '';
        
        if (! is_string($username)) {
            $username = '';
        }

        return $username;
    }

    public function logout(): void
    {
        $this->session->clear();
    }
}