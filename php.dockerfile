FROM php:8.2 as php

RUN apt-get update -y
RUN apt-get install -y unzip \
    nmap \
    libpq-dev \
    libcurl4-gnutls-dev
RUN docker-php-ext-install pdo pdo_mysql bcmath

RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

WORKDIR /var/www

COPY . .