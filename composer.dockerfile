FROM composer

WORKDIR /var/www

COPY . .

RUN composer install --no-interaction

RUN composer dump-autoload