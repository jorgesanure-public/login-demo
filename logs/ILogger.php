<?php

namespace App\Logs;

interface ILogger
{
    public function log(?string $message): void;
    public function logError(?string $message, ?string $file = null, ?int $line = null): void;
    public function logWarning(?string $message, ?string $file = null, ?int $line = null): void;
}