<?php

namespace App\Logs;

class OldLogFileRemover implements IOldLogFileRemover
{
    public function remove(string $directory, ILogger $logger): void
    {
        if (is_dir($directory)) {
            $files = scandir($directory);

            if ($files !== false) {
                $filesToIgnoreFromDelete = ['.', '..', 'readme.md'];
                $files = array_diff($files, $filesToIgnoreFromDelete);
                $files = array_filter($files, function ($file) use ($directory) {
                    return is_file($directory . '/' . $file);
                });

                $currentDate = date('Y-m-d');
                foreach ($files as $file) {
                    if (
                        strpos($file, $currentDate) !== false
                        || empty(preg_match('/\d{4}-\d{2}-\d{2}/', $file))
                    ) {
                        continue;
                    }

                    $filename = $directory . '/' . $file;
                    if (
                        file_exists($filename)
                        && unlink($filename)
                    ) {
                        $logger->log("File $file has been deleted.");
                    }
                }
            }
        }
    }
}