<?php

namespace App\Logs;

use App\Services\IEnv;
use App\Services\IStringUtils;

abstract class BaseLogger implements ILogger
{
    private IOldLogFileRemover $oldLogFileRemover;
    private IStringUtils $stringUtils;
    private IEnv $env;
    
    public function __construct(IOldLogFileRemover $oldLogFileRemover, IStringUtils $stringUtils, IEnv $env)
    {
        $this->oldLogFileRemover = $oldLogFileRemover;
        $this->stringUtils = $stringUtils;
        $this->env = $env;
    }

    protected function getCurrentLogFilename(): string
    {
        return 'log';
    }

    private function getCurrentLogWithWarningFilename(): string
    {
        return 'log_with_warning';
    }

    private function getLogDirectory(): string
    {
        return __DIR__ . '/log_files';
    }

    private function buildFingerprint(): string
    {
        return '[' . date('Y-m-d H:i:s T') . '] ';
    }

    private function buildFilenameDateSuffix(): string
    {
        return date('Y-m-d');
    }

    public function log(?string $data): void
    {
        $this->_log($data);
    }

    public function logError(?string $data, ?string $file = null, ?int $line = null): void
    {
        $this->_log("[ERROR] " . (empty($file) ? '' : "$file:$line ") . $data);
    }

    public function logWarning(?string $data, ?string $file = null, ?int $line = null): void
    {
        if ($this->env->get('LOG_WITH_WARNING', '0') == 1) {
            $this->_log("[WARNING] " . (empty($file) ? '' : "$file:$line ") . $data, $this->getCurrentLogWithWarningFilename());
        }
    }

    protected function buildLogFilename(string $logName, bool $includeDate = true): string
    {
        if ($includeDate) {
            $logName .= '_' . $this->buildFilenameDateSuffix();
        }

        return $logName . '.txt';
    }

    private function _log(?string $data, ?string $logFile = null, string $mode = 'a', bool $warningFlag = false, ?string $logId = null): void
    {
        $logId ??= str_replace('.', '', (string)microtime(true));

        // to log errors into warning log as well to co-relate both of them easier
        if (
            $this->env->get('LOG_WITH_WARNING', '0') == 1
            && $warningFlag === false
            && (
                $logFile === null
                || $logFile === $this->getCurrentLogFilename()
            )
        ) {
            $this->_log($data, $this->getCurrentLogWithWarningFilename(), $mode, true, $logId);
        }
        
        $data = $this->buildFingerprint() . "[LogID:$logId] " . ($data ?? '');

        $logFile = $this->getLogDirectory() . '/' . $this->buildLogFilename($logFile ?? $this->getCurrentLogFilename());

        $this->oldLogFileRemover->remove($this->getLogDirectory(), $this);

        // remove breaklines
        $data = $this->stringUtils->removeBreaklines($data);
        $data = $this->stringUtils->removeExtraWhitespace($data);

        $fh = fopen($logFile, $mode);
        if ($fh !== false) {
            if (! empty($data)) {
                $data .= PHP_EOL;
                fwrite($fh, $data);
            }

            fclose($fh);
        }
    }
}