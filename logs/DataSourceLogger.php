<?php

namespace App\Logs;

class DataSourceLogger extends BaseLogger implements IDataSourceLogger
{
    protected function getCurrentLogFilename(): string
    {
        return 'data_source_log';
    }
}