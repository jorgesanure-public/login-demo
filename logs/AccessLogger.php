<?php

namespace App\Logs;

class AccessLogger extends BaseLogger implements IAccessLogger
{
    protected function getCurrentLogFilename(): string
    {
        return 'access_log';
    }
}