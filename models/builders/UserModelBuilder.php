<?php

namespace App\Models\Builders;

use App\Models\UserModel;

class UserModelBuilder implements IUserModelBuilder
{
    public function build(?array $data): ?UserModel
    {
        $user = null;

        if (! empty($data)) {
            $user = new UserModel();
            $user->username = array_key_exists('username', $data) ? $data['username'] : null;
            $user->password = array_key_exists('password', $data) ? $data['password'] : null;
            $user->accountCode = array_key_exists('accountCode', $data) ? $data['accountCode'] : null;
        }

        return $user;
    }
}