<?php

namespace App\Models\Builders;

use App\Models\UserModel;

interface IUserModelBuilder
{
    /** @param array<mixed> $data */
    public function build(?array $data): ?UserModel;
}