<?php

namespace App\Models;

class UserModel
{
    public ?string $username;
    public ?string $password;
    public ?string $accountCode;

    public function __construct(?string $username = null, ?string $password = null, ?string $accountCode = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->accountCode = $accountCode;
    }
}