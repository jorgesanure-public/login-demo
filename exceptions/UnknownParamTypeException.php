<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class UnknownParamTypeException extends Exception
{
    public function __construct(string $class, string $paramName, int $code = 1, Throwable|null $previous = null) {
        parent::__construct("Unknown param type from constructor of '$class' with param name '$paramName'", $code, $previous);
    }
}