<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class UsernameAlreadyExistsException extends Exception
{
    public function __construct(string $message = "Username already exists", int $code = 1, Throwable|null $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}