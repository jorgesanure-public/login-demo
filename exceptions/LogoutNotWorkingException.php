<?php

class LogoutNotWorkingException extends Exception
{
    public function __construct(string $message = "Error Processing Logout. User keep logged in even after logout.", int $code = 1, Throwable|null $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}