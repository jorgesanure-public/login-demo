<?php

namespace App\Views;

interface IView
{
    /** @param array<string, mixed> $viewParams */
    public function build(string $viewFilename, array $viewParams = []): void;
    public function set(string $key, mixed $value): void;
    public function get(string $key): mixed;
}