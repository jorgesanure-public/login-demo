<?php
    /** @var ?string $userErrorMsg */
    /** @var ?string $loginUsername */
?>
<div class="container mt-5">
    <div class="row">
        <div class="d-none d-sm-block col-sm-1 col-md-2 col-lg-3"></div>
        <div class="col-sm-10 col-md-8 col-lg-6">
            <div class="card p-3">
                <div class="row">
                    <div class="col">
                        <h1>Login Form</h1>
                    </div>
                </div>
                <div class="row mt-4">
                    <form action="/login" method="post">
                        <?php
                            if(! empty($userErrorMsg)) { 
                        ?>
                            <div class="alert alert-danger">
                                <?= $userErrorMsg; ?>
                            </div>
                        <?php } ?>
                        <div class="mb-3">
                            <label for="username" class="form-label">Username:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="bi bi-person"></i>
                                </span>
                                <input type="text" class="form-control" name="username" id="username" aria-describedby="username-help" placeholder="Username" value="<?= $loginUsername ?>">
                            </div>
                            <small id="username-help" class="form-text text-muted">Username can be your name, alias or email</small>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password:</label>
                            <div class="input-group">
                                <span class="input-group-text">
                                    <i class="bi bi-lock"></i>
                                </span>
                                <input type="password" class="form-control" name="password" id="password" placeholder="***">
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" name="login-btn" id="login-btn" class="btn btn-primary">Submit</button>
                            <a name="back-btn" id="back-btn" class="btn btn-outline-secondary" href="/" role="button">Back</a>
                            <div class="mt-3">
                                <a href="/signup" style="text-decoration:none;">Sign Up</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="d-none d-sm-block col-sm-1 col-md-2 col-lg-3"></div>
    </div>
</div>