<?php

namespace App\Views;

use Exception;

class View implements IView
{
    /** @var array<string, mixed> $viewParams */
    private array $viewParams;

    public function __construct()
    {
        $this->viewParams = [];
    }

    public function build(string $viewFilename, array $viewParams = []): void
    {
        $viewParams = array_merge($this->viewParams, $viewParams);

        if (! empty($viewParams)) {
            extract($viewParams);
        }

        $viewFilename = __DIR__ . '/' . $viewFilename;

        if (file_exists($viewFilename)) {
            ob_start();
            require_once __DIR__ . '/templates/header.php';
            require_once $viewFilename;
            require_once __DIR__ . '/templates/footer.php';
            $htmlContent = ob_get_clean();
            header('Content-Type: text/html');
            echo $htmlContent;
        } else {
            throw new Exception("File not found while building view: '$viewFilename'", 1);
        }
    }

    public function set(string $key, mixed $value): void
    {
        $this->viewParams[$key] = $value;
    }

    public function get(string $key): mixed
    {
        $val = null;

        if (array_key_exists($key, $this->viewParams)) {
            $val = $this->viewParams[$key];
        }

        return $val;
    }
}