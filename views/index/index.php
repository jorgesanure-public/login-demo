<?php
    /** @var IAuthenticator $authenticator */
?>
<div class="mt-2 mb-3">
    <?php if ($authenticator->isAuthenticated()) { ?>
        <div class="float-end">
            <div class="d-inline">
                <?= $authenticator->getUsername(); ?>
                <a name="logout-btn" id="logout-btn" class="btn btn-sm btn-outline-danger" href="/logout" role="button">Logout</a>
            </div>
        </div>
    <?php } else { ?>
        <div class="float-end">
            <div class="d-inline">
                <a name="login-btn" id="login-btn" class="btn btn-sm btn-outline-primary" href="/login" role="button">Login</a>
            </div>
            <div class="d-inline">
                <a name="signup-btn" id="signup-btn" class="btn btn-sm btn-outline-secondary" href="/signup" role="button">Sign Up</a>
            </div>
        </div>
    <?php } ?>
    <div>
        <h1>Login Demo</h1><span><a href="https://gitlab.com/jorgesanure-public/login-demo">Gitlab Repository</a></span>
        <p>This Php project is a login-demo showcase that include:</p>
        <ul class="list-group">
            <li class="list-group-item list-group-item-primary list-group-item-action">
                <h4>Front-End</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">Bootstrap</li>
                    <li class="list-group-item list-group-item-action">CSS</li>
                    <li class="list-group-item list-group-item-action">HTML</li>
                    <li class="list-group-item list-group-item-action">Javascript</li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-info list-group-item-action">
                <h4>Back-End</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">Architecture
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">MVC</li>
                            <li class="list-group-item list-group-item-action">Repositories</li>
                            <li class="list-group-item list-group-item-action">Dependency Inversion</li>
                            <li class="list-group-item list-group-item-action">IoC</li>
                            <li class="list-group-item list-group-item-action">Dependency Injection</li>
                            <li class="list-group-item list-group-item-action">SOLID</li>
                        </ul>
                    </li>
                    <li class="list-group-item list-group-item-action">Features
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item list-group-item-action">Logger
                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-action">System Logger</li>
                                    <li class="list-group-item list-group-item-action">Deploy Logger</li>
                                    <li class="list-group-item list-group-item-action">Datasource Logger</li>
                                </ul>
                            </li>
                            <li class="list-group-item list-group-item-action">Router</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-primary list-group-item-action">
                <h4>Database</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">Sqlite3</li>
                    <li class="list-group-item list-group-item-action">Migrations (PHINX)</li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-info list-group-item-action">
                <h4>Version Control</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">GIT</li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-primary list-group-item-action">
                <h4>Testing</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">PHPUnit</li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-info list-group-item-action">
                <h4>Lint/Code Quality Check</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action">PHPStan</li>
                </ul>
            </li>
            <li class="list-group-item list-group-item-primary list-group-item-action">
                <h4>Env Var</h4>
            </li>
            <li class="list-group-item list-group-item-info list-group-item-action">
                <h4>XAMP</h4>
            </li>
        </div>
    </div>
</div>