<?php
    /** @var ?string $username */
?>
<div class="mt-2">
    <div class="float-start">
        <div>
            Welcome: <?= $username ?>
        </div>
        <div>
            Dashboard...
        </div>
    </div>
    <div class="float-end">
        <div class="d-inline">
            <a name="logout-btn" id="logout-btn" class="btn btn-sm btn-outline-danger" href="/logout" role="button">Logout</a>
        </div>
    </div>
</div>