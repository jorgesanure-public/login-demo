<?php

use App\Constants\RequestMethod;
use App\Controllers\IDashboardController;
use App\Controllers\IDeployController;
use App\Controllers\IIndexController;
use App\Controllers\ILoginController;
use App\Controllers\ISignUpController;
use App\Exceptions\ServiceProviderNotFoundException;
use App\Routes\IRouter;
use App\Routes\Middleware;
use App\Routes\Route;
use App\Services\IAuthenticator;
use App\Services\IRequest;
use App\Services\IResponse;

if (! isset($serviceProvider)) throw new ServiceProviderNotFoundException();

/** @var IRouter $router */
$router = $serviceProvider->resolve(IRouter::class);

Middleware::set('auth', function () use ($serviceProvider) {
    $isAuthenticated = $serviceProvider->resolve(IAuthenticator::class)->isAuthenticated();
    $request = $serviceProvider->resolve(IRequest::class);
    $response = $serviceProvider->resolve(IResponse::class);

    if (! $isAuthenticated) {
        if ($request->isApi()) {
            $response->responseUnauthorized();
        } else {
            $request->redirectTo('/login');
        }
    }

    return $isAuthenticated;
});

Middleware::set('guest', function () use ($serviceProvider) {
    $isAuthenticated = $serviceProvider->resolve(IAuthenticator::class)->isAuthenticated();
    $request = $serviceProvider->resolve(IRequest::class);
    $response = $serviceProvider->resolve(IResponse::class);

    if ($isAuthenticated) {
        if ($request->isApi()) {
            $response->responseUnauthorized('You must to logout first.');
        } else {
            $request->redirectTo('/dashboard');
        }
    }

    return ! $isAuthenticated;
});

$router->addRoute(
    Route::create()
        ->setRouteName('/')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IIndexController::class)->index();
        })
);

$router->addRoute(
    Route::create()
        ->setRouteName('/login')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->showLoginForm();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/login')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->login();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/login')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->login();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/logout')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->logout();
        })
        ->setMiddlewares(['auth'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/logout')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ILoginController::class)->logout();
        })
        ->setMiddlewares(['auth'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/signup')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->showSignUpForm();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/signup')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->signup();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/api/signup')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(ISignUpController::class)->signup();
        })
        ->setMiddlewares(['guest'])
);

$router->addRoute(
    Route::create()
        ->setRouteName('/deploy')
        ->setMethod(RequestMethod::POST)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IDeployController::class)->deploy();
        })
        ->setShowOnSitemap(false)
        ->setRequireSession(false)
);

$router->addRoute(
    Route::create()
        ->setRouteName('/dashboard')
        ->setMethod(RequestMethod::GET)
        ->setCallback(function() use ($serviceProvider) {
            $serviceProvider->resolve(IDashboardController::class)->index();
        })
        ->setMiddlewares(['auth'])
);