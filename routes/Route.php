<?php

namespace App\Routes;

use App\Constants\RequestMethod;
use Exception;

class Route implements IRoute
{
    private ?string $routeName;
    /** @var ?callable $callback */
    private mixed $callback;
    private ?RequestMethod $method;
    /** @var array<string> $middlewares */
    private array $middlewares;
    private bool $showOnSitemap;
    private bool $requireSession;

    public function __construct()
    {
        $this->showOnSitemap = true;
        $this->requireSession = true;
        $this->middlewares = [];
        $this->callback = null;
    }

    public function setCallback(callable $callback): IRoute
    {
        $this->callback = $callback;
        return $this;
    }

    public function getCallback(): ?callable
    {
        return $this->callback;
    }

    public function isPublic(): bool
    {
        return empty($this->getMiddlewares()) || ! in_array('auth', $this->getMiddlewares());
    }

    public function showOnSitemap(): bool
    {
        return (bool) $this->showOnSitemap && $this->isPublic() && $this->isGet();
    }

    public function setShowOnSitemap(bool $show): IRoute
    {
        $this->showOnSitemap = $show;
        return $this;
    }

    public function isPost(): bool
    {
        return $this->getMethod() === RequestMethod::POST;
    }

    public function isGet(): bool
    {
        return $this->getMethod() === RequestMethod::GET;
    }

    public function getMethod(): ?RequestMethod
    {
        return $this->method;
    }

    public function getRouteName(): ?string
    {
        return $this->routeName;
    }

    public function requireSession(): bool
    {
        return (bool) $this->requireSession;
    }

    public function setRouteName(string $name): IRoute
    {
        $this->routeName = $name;
        return $this;
    }

    public function setRequireSession(bool $require): IRoute
    {
        $this->requireSession = $require;
        return $this;
    }

    public function route(): void
    {
        if ($this->requireSession()) {
            session_start();
        }

        $middlewareCheck = empty($this->getMiddlewares());

        if (! $middlewareCheck) {
            $middlewareCheck = true;
            foreach ($this->getMiddlewares() as $middlewareName) {
                $middleware = Middleware::get($middlewareName);
                $middlewareCheck = $middlewareCheck && ! empty($middleware) && $middleware->check();
            }
        }

        if ($middlewareCheck && is_callable($this->callback)) {
            ($this->callback)();
        } else {
            throw new Exception("Error Processing Request", 1);
        }
    }

    public function setMethod(RequestMethod $method): IRoute
    {
        $this->method = $method;
        return $this;
    }

    public static function create(): IRoute
    {
        return new Route();
    }

    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

    public function setMiddlewares(array $middlewares): IRoute
    {
        $this->middlewares = $middlewares;
        return $this;
    }
}