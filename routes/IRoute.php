<?php

namespace App\Routes;

use App\Constants\RequestMethod;

interface IRoute
{
    public function requireSession(): bool;
    public function setRequireSession(bool $require): IRoute;
    public function route(): void;
    public function isPublic(): bool;
    public function showOnSitemap(): bool;
    public function setShowOnSitemap(bool $show): IRoute;
    public function isPost(): bool;
    public function isGet(): bool;
    public function getRouteName(): ?string;
    public function setRouteName(string $name): IRoute;
    public function getMethod(): ?RequestMethod;
    public function setMethod(RequestMethod $method): IRoute;
    public function setCallback(callable $callback): IRoute;
    public function getCallback(): ?callable;
    /** @param array<string> $middlewares */
    public function setMiddlewares(array $middlewares): IRoute;
    /** @return array<string> */
    public function getMiddlewares(): array;
}