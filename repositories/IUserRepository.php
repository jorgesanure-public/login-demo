<?php

namespace App\Repositories;

use App\Models\UserModel;

interface IUserRepository
{
    public function existsUserWithUsername(string $username): bool;
    public function signUp(UserModel $user): void;
    public function findByUsername(string $username): ?UserModel;
}