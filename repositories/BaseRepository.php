<?php

namespace App\Repositories;

use App\Db\CustomSQLite3;

abstract class BaseRepository
{
    protected CustomSQLite3 $dataSource;

    public function __construct(CustomSQLite3 $dataSource)
    {
        $this->dataSource = $dataSource;
    }
}