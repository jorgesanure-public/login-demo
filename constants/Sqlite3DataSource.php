<?php

namespace App\Constants;

class Sqlite3DataSource
{
    public const PRODUCTION = 'production';
    public const DEVELOPMENT = 'development';
    public const TESTING = 'testing';
}