<?php

namespace App\Constants;

enum RequestMethod: string
{
    case GET = 'GET';
    case POST = 'POST';
}