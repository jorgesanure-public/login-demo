<?php

namespace App\Constants;

class ServiceLifetimeType
{
    public const TRANSIENT = 'transient';
    public const SINGLETON = 'singleton';
}